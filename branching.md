# Branches, Commits, and Merges

## Creating a new branch

From our new project, we can create a new branch to separate new code changes from the master branch.

![alt text](figures/new_branch1.png)

We need to give the new branch a new and chose what branch it stems from.

![alt text](figures/new_branch2.png)

## Editing a file in a commit

We add pieces of code to a branch in a "Commit." From the commandline, we can stage many files into a single commit. 
From the web interface, however, we can only add/edit a single file in one commit. For the purposes of this project, this is good enough.

![alt text](figures/new_file1.png)

From here we can name our file, add some text, and add a commit message to *briefly* describe our edit.

![alt text](figures/new_file2.png)

## Creating a merge request

Up to this point, our new file only exists in a new branch. To merge the changes in our branch back into the master branch, we need to do a merge request.

![alt text](figures/merge_request1.png)

We need to give our merge request a title, a description, and which branch we are merging (which should already be there).

![alt text](figures/merge_request2.png)

Once we've created our merge request, members of the project can be comments on the merge request and specific lines in the merge request. 
You can also complete the merge request and merge the branch from here.

![alt text](figures/merge_request3.png)

The GitLab interface also provides a way to make commits with edits to single files in the branch. 
Otherwise, you can checkout the branch corresponding to the merge request in order to make larger commits to the branch.

![alt text](figures/merge_request4.png)