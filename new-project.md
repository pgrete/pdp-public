# Getting Started

## Creating a new Project

From the start page of GitLab, you can start a new project to begin testing. (This screen will be a little different if you've already made a project before).

![alt text](figures/gitlab_start.png)

You can give your new project a name, a description, and set the level of public access.

![alt text](figures/new_project.png)

## Add members to your project

To collaborate with your team members, you'll need to add them as project members under Settings.

![alt text](figures/add_member1.png)

You can look for members by their GitLab user name and give them a level of permissions. "Maintainer" will be best for this project.

![alt text](figures/add_member2.png)